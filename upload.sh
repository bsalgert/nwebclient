#!/bin/sh

PYBIN=python

# pip3.8 install twine

echo "Clean"
cd dist && rm * && cd ..

echo "Building Package..."
$PYBIN setup.py sdist bdist_wheel

echo "Upload"
echo ""
echo " See GitLab-Wiki for Details"
echo ""
$PYBIN -m twine upload dist/*

git add .
git commit -m "$($PYBIN version.py)"
git push
