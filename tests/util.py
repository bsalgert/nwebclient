import unittest

from nwebclient import util as u

class BaseTestCase(unittest.TestCase):
    
    def test_Args(self):
        args = u.Args.from_cmd('docker run -it --name myubuntu')
        self.assertEqual(args['name'], 'myubuntu')


if __name__ == '__main__':
    unittest.main()
