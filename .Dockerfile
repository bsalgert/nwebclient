FROM nxware/nxdev
COPY . /nwebclient
RUN pip install --upgrade --break-system-packages /nwebclient
EXPOSE 7070
CMD ["python", "-m", "nwebclient.runner"]